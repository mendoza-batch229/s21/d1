// console.log("Hello AL!");


// An array in programming isi simply a list of data.
let studentNumber1 = "2020-1923";

// Now with array we can simply write  the code above like this.
let studentNumbers = ['2020-1923','2020-1924','2020-1925','2020-1926','2020-1927'];

// [SECTION] - Array
// Arrays are use to store multiple related values in a single variable.
// They are declares using "[]" brackets also known as array literals.
// SYNTAX -> let/const arrayName = ['valueA', 'valueB', ...]


//Ccommon example
let grades = [98, 94, 89, 90];
let computerBrands = ["acer", "asus", "Lenove", "Neo", "redfox", "gateway", "Toshiba", "Fujitsu"];

// Possible use of array but not recomended
let mixedArr = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write arrays
let myTasks = [
	"drink html",
	"eat JS",
	"inhale CSS",
	"bake SASS"
	];

// Creating an array with values from variables

let city1="tokyo";
let city2 = "Manila";
let city3 = "Jakrta";

let cities = [city1,city2,city3];
console.log(myTasks);
console.log(cities);

// [section] -length property
// The .length property allows us to get and set the total number of items in an array.

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);


// Let property can also be used with string even space and anyu other special characters

let fullName = "Jamie Noble";
console.log(fullName.length);

// length property can also set the total number of items in an array
myTasks.length = myTasks.length -1;
console.log(myTasks.length);
console.log(myTasks);

// To delete specific item/value in an array we can employ array methods(which will be shown in the next session)

// Another example using decrementation
cities.length--;
console.log(cities);

// We can,t do the same thing on strings, however.
fullName.length = fullName.length -1;
console.log(fullName.length);

fullName.length--;
console.log(fullName);

// If you can shorten the array by setting the length property, you can also lengthen it by adding into the length property.

let theBeatles =["john", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// [SECTION] - READING GROM ARRAY
/*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed
- Array indexes actually refer to an address/location in the device's memory and how the information is stored
    - Example array location in memory
        Array address: 0x7ffe9472bad0
        Array[0] = 0x7ffe9472bad0
        Array[1] = 0x7ffe9472bad4
        Array[2] = 0x7ffe9472bad8
    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
- Syntax
        arrayName[index];
*/


console.log(grades[0]);
// accessing an array element that doed not exist will return undefined
console.log(grades[10]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
// Access the 2nd item in the array
console.log(lakersLegends[1]) //Shaq

// Access the 4th item in the array
console.log(lakersLegends[3]) //Shaq

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also re-assign array clues using the item in indeces
console.log("Array before re-assignment");
console.log(lakersLegends);
// re-assignment method
console.log("Array after re-assignment");
lakersLegends[2] = "Gasol";
console.log(lakersLegends);

// Accessing the last elements of an array 
// Since the first element start at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element

let bullsLegends =["Jordan", "Pippen", "Rodman", "Rose",
					"Kukoc"];
let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex]);

// Adding items into the array
// Using indeces, you can also add item into the array
let newArray = [];
console.log(newArray[0]);

newArray[0] = "Cloud Strife";
console.log(newArray);

console.log(newArray[1]);

newArray[1] = "Tifa Lockhart";
console.log(newArray);

newArray[newArray.length] = "Barret Wallace";
console.log(newArray);


// Looping over an array


let numbers = [5, 12, 30, 46, 40]

for(let index = 0; index < numbers.length; index++){
	if(numbers[index] % 5 === 0){
		console.log(numbers[index] + " is divisble by 5");
	}else{
		console.log(numbers[index] + " is not divisible by 5");
	}
}



// [SECTION] - MULTIDIMENSIONAL ARRAYS
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.table(chessBoard);

// Accessing an elements of a multi dimensional arrays
// [1] -> Column [4] ->Row
console.log(chessBoard[1][4]);


console.log("Pawan move to: " + chessBoard[1][5]);

